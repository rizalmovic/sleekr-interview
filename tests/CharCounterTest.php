<?php
use PHPUnit\Framework\TestCase;
use Sleekr\CharCounter;

class CharCounterTest extends TestCase
{
    public function testFirst()
    {
        $sentence = "abcde";
        $counter = new CharCounter($sentence);
        $counter->compute();
        $this->assertEquals(5, $counter->getLength());
        $this->assertEquals(2, $counter->getVocal());
        $this->assertEquals(3, $counter->getConsonant());
        $this->assertEquals(6, $counter->getVocal() * $counter->getConsonant());
    }

    public function testSecond()
    {
        $sentence = "Jangan buang sampah sembarangan!";
        $counter = new CharCounter($sentence);
        $counter->compute();
        $this->assertEquals(28, $counter->getLength());
        $this->assertEquals(10, $counter->getVocal());
        $this->assertEquals(18, $counter->getConsonant());
        $this->assertEquals(180, $counter->getVocal() * $counter->getConsonant());
    }

    public function testThird()
    {
        $sentence = "098f6bcd4621d373cade4e832627b4f6";
        $counter = new CharCounter($sentence);
        $counter->compute();
        $this->assertEquals(12, $counter->getLength());
        $this->assertEquals(3, $counter->getVocal());
        $this->assertEquals(9, $counter->getConsonant());
        $this->assertEquals(27, $counter->getVocal() * $counter->getConsonant());
    }

    public function testFourth()
    {
        $sentence = "abcde";
        $counter = new CharCounter($sentence);
        $counter->other_compute();
        $this->assertEquals(5, $counter->getLength());
        $this->assertEquals(2, $counter->getVocal());
        $this->assertEquals(3, $counter->getConsonant());
        $this->assertEquals(6, $counter->getVocal() * $counter->getConsonant());
    }

    public function testFifth()
    {
        $sentence = "Jangan buang sampah sembarangan!";
        $counter = new CharCounter($sentence);
        $counter->other_compute();
        $this->assertEquals(28, $counter->getLength());
        $this->assertEquals(10, $counter->getVocal());
        $this->assertEquals(18, $counter->getConsonant());
        $this->assertEquals(180, $counter->getVocal() * $counter->getConsonant());
    }

    public function testSixth()
    {
        $sentence = "098f6bcd4621d373cade4e832627b4f6";
        $counter = new CharCounter($sentence);
        $counter->other_compute();
        $this->assertEquals(12, $counter->getLength());
        $this->assertEquals(3, $counter->getVocal());
        $this->assertEquals(9, $counter->getConsonant());
        $this->assertEquals(27, $counter->getVocal() * $counter->getConsonant());
    }
}