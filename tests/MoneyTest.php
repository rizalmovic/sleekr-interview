<?php
use PHPUnit\Framework\TestCase;
use Sleekr\Money;

class MoneyTest extends TestCase
{
    public function testSatuan()
    {
        $amount = 7;
        $money = new Money();
        $this->assertEquals("tujuh", $money->toString($amount));
    }

    public function testBelasan()
    {
        $money = new Money();

        $amount = 11;
        $this->assertEquals("sebelas", $money->toString($amount));

        $amount = 17;
        $this->assertEquals("tujuh belas", $money->toString($amount));
    }

    public function testPuluhan()
    {
        $money = new Money();

        $amount = 10;
        $this->assertEquals("sepuluh", $money->toString($amount));

        $amount = 21;
        $this->assertEquals("dua puluh satu", $money->toString($amount));

        $amount = 91;
        $this->assertEquals("sembilan puluh satu", $money->toString($amount));
    }

    public function testRatusan()
    {
        $money = new Money();

        $amount = 102;
        $this->assertEquals("seratus dua", $money->toString($amount));

        $amount = 125;
        $this->assertEquals("seratus dua puluh lima", $money->toString($amount));

        $amount = 525;
        $this->assertEquals("lima ratus dua puluh lima", $money->toString($amount));
    }

    public function testRibuan()
    {
        $money = new Money();

        $amount = 2500;
        $this->assertEquals("dua ribu lima ratus", $money->toString($amount));

        $amount = 1988;
        $this->assertEquals("seribu sembilan ratus delapan puluh delapan", $money->toString($amount));
    }

    public function testPuluhRibuan()
    {
        $money = new Money();

        $amount = 32500;
        $this->assertEquals("tiga puluh dua ribu lima ratus", $money->toString($amount));

        $amount = 12500;
        $this->assertEquals("dua belas ribu lima ratus", $money->toString($amount));
    }

    public function testRatusRibuan()
    {
        $money = new Money();

        $amount = 155025;
        $this->assertEquals("seratus lima puluh lima ribu dua puluh lima", $money->toString($amount));

        $amount = 650750;
        $this->assertEquals("enam ratus lima puluh ribu tujuh ratus lima puluh", $money->toString($amount));
    }

    public function testJutaan()
    {
        $money = new Money();

        $amount = 9575000;
        $this->assertEquals("sembilan juta lima ratus tujuh puluh lima ribu", $money->toString($amount));

        $amount = 1500000;
        $this->assertEquals("satu juta lima ratus ribu", $money->toString($amount));
    }

    public function testPuluhanJutaan()
    {
        $money = new Money();

        $amount = 90575000;
        $this->assertEquals("sembilan puluh juta lima ratus tujuh puluh lima ribu", $money->toString($amount));
    }

    public function testRatusanJutaan()
    {
        $money = new Money();

        $amount = 950000000;
        $this->assertEquals("sembilan ratus lima puluh juta", $money->toString($amount));
    }

    public function testMilyar()
    {
        $money = new Money();

        $amount = 1500000000;
        $this->assertEquals("satu milyar lima ratus juta", $money->toString($amount));
    }
}