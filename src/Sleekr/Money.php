<?php
namespace Sleekr;

class Money
{
    private $dictionary;
 
    public function __construct()
    {
        $this->dictionary = ["","satu","dua","tiga","empat","lima","enam","tujuh","delapan","sembilan", "sepuluh", "sebelas"];
    }

    public function toString($amount)
    {
        $result = "";
        switch (true) {
            case $amount < 12:
                $result = $this->dictionary[$amount];
                break;
            case $amount < 20:
                $result = $this->toString($amount % 10) . ' belas';
                break;
            case $amount < 100:
                $result = $this->toString($amount / 10) . ' puluh ' . $this->toString($amount % 10);
                break;
            case $amount < 200:
                $result = 'seratus ' . $this->toString( $amount % 100 );
                break;
            case $amount < 1000:
                $result = $this->dictionary[ $amount / 100 ] . ' ratus ' . $this->toString( $amount % 100 );
                break;
            case $amount < 2000:
                $result = 'seribu ' . $this->toString( $amount % 1000 );
                break;
            case $amount < 1000000:
                $result = $this->toString($amount / 1000) . ' ribu ' . $this->toString( $amount % 1000 );
                break;
            case $amount < 1000000000:
                $result = $this->toString($amount / 1000000) . ' juta ' . $this->toString( $amount % 1000000 );
                break;
            case $amount < 1000000000000:
                $result = $this->toString($amount / 1000000000) . ' milyar ' . $this->toString( $amount % 1000000000);
                break;
            default:
                $result = "";
                break;
        }

        return trim($result);
    }
}