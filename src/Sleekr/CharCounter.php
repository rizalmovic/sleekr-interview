<?php 

namespace Sleekr;

class CharCounter
{
    private $sentence;
    private $characters;
    private $dictionary;
    private $vocal;
    private $consonant;
    private $sentenceLength;

    public function __construct($sentence = '')
    {
        $this->dictionary = ['a','i','u','e','o'];
        $this->vocal = 0;
        $this->consonant = 0;
        $this->sentence = $sentence;
        $this->sentence = strtolower(preg_replace("/[^a-z]/i", "", $this->sentence));
        $this->sentenceLength = strlen($this->sentence);
        
        // Uncomment one of below typw of compute to automate compute on object construction.
        // $this->compute();
        // $this->other_compute();
    }

    public function compute()
    {
        $this->characters = str_split($this->sentence);        

        foreach($this->characters as $char)
        {
            if(in_array($char, $this->dictionary))
            {
                $this->vocal++;
            }
            else
            {
                $this->consonant++;
            }
        }
    }

    public function other_compute()
    {
        preg_match_all("/[a,i,u,e,o]/i", $this->sentence, $matches);

        $this->vocal = (count($matches)) ? count($matches[0]) : 0;
        $this->consonant = $this->sentenceLength - $this->vocal;
    }

    public function getLength()
    {
        return $this->sentenceLength;
    }

    public function getVocal()
    {
        return $this->vocal;
    }

    public function getConsonant()
    {
        return $this->consonant;
    }
}